import { IsNotEmpty, MinLength, MaxLength } from "class-validator";

export class RegisterDto {

    @IsNotEmpty()
    @MinLength(3)
    @MaxLength(30)
    username: string;

    @IsNotEmpty()
    @MinLength(8)
    password: string;
}