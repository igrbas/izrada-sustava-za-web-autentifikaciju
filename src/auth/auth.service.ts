import { ConflictException, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { User, UserDocument } from "src/schemas/user.schema";
import * as bcrypt from "bcrypt";
import { RegisterDto } from "./dtos/register.dto";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>, private jwtService: JwtService) {}

    async validate(username: string, password: string) {
        const user = await this.userModel.findOne({ username });

        if(user && bcrypt.compareSync(password, user.password)) {
            user.password = undefined;
            return user;
        }

        return null;
    }

    async register(registerDto: RegisterDto) {
        const user = await this.userModel.findOne({ username: registerDto.username });

        if(user) {
            throw new ConflictException("User already exists");
        }

        registerDto.password = await bcrypt.hash(registerDto.password, 12);
        await new this.userModel(registerDto).save();
        
        return "User registered"; 
    }

    async createJWTToken(user: any) {
        return {
            access_token: this.jwtService.sign({ id: user._id, username: user.username })
        }
    }
}