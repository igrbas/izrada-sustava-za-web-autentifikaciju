<h1>Upute za korištenje programske podrške</h1>

Za pokretanje programa potrebno je skinuti:<br>
<ul>
    <li>1. NodeJS LTS verzija https://nodejs.org/en/</li>
    <li>2. NestJS s naredbom: npm i -g @nestjs/cli</li>
    <li>3. MongoDB https://www.mongodb.com/</li>
</ul>

Nakon toga, potrebno je u terminalu pozicionirati se u direktoriji programa i pokrenuti sljedeće naredbe:<br>
<ul>
    <li>1. npm install - preuzimanje svih potrebnih biblioteka</li>
    <li>2. npm run start - pokretanje programa</li>
</ul>

Za testiranje programa može se korisiti program Postman. Postman je program koji služi za testiranje API-a.
https://www.postman.com/

Za registraciju korisnika potrebno je napravit http POST zahtjev na http://localhost:3000/auth/register . U tijelo (body) zahtjeva treba
prosljediti sljedeće podatke u JSON obliku:<br>
{<br>
    "username": "korisničko ime",<br>
    "password": "lozinka123"<br>
}<br>

Prijava u sustav je ista kao i registracija, jedino je url drugačiji: http://localhost:3000/auth/login

Nakon uspješne prijave, dobiven pristupni token potrebno je staviti u zaglavlje http zahtjeva kao Bearer token.
Authorization - Bearer "pristupni token"

Na kraju, GET http zahtjev na http://localhost:3000 trebao bi vratiti "Hello world"
